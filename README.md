# README #

## Brief description##
**bOrganizer** is yet another GTD-like web app. I build it with heavy focus on using gamification techniques while working by [Pomodoros](http://pomodorotechnique.com/)

## Why this project exists? ##
People try and build new GTD and productivity items every day now.

I believe there is a good reason for all those apps to exist - every one of them provide slightly different approach and suit slightly different needs.

I've also tried a lot of them and now I decided to try and invent a wheel that will suit my personal needs best.

### How do I get set up and contribute? ###
You probably don't want to do so. This project is in the very beginning of it's development and only has basic functionality any other GTD app will provide for sure.

However, I have a big plans for this one so if you absolutely want to contribute - contact me and we'll figure something out. Or just check out this repo in half a year - I hope to finish most important parts by that time